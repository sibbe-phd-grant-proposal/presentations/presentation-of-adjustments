# FGM-presentation-2024-02-06

The presentation for my
[RMC](https://git.wur.nl/sibbe-rmc/presentations/presentation-of-adjustments)^[Email
me to request access] course. The presentation is hosted
[here](https://bookdown.org/sibbe_l_bakker/rmc-adjustments-presentation-dev/).
